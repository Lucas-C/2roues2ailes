#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
from os.path import dirname, join

SITENAME = '2 roues 2 ailes'
SITESUBTITLE = 'EuroVelo6 nous voici !'
DESCRIPTION = "Blog de notre aventure estivale à vélo jusqu'à la mer noire"

TIMEZONE = 'Europe/Paris'
DEFAULT_LANG = 'fr'

PATH = './content'
ISSO_BASE_URL = 'https://chezsoi.org/lucas/isso'
NOINDEX = True

MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.codehilite': {'css_class': 'highlight'},
        'markdown.extensions.extra': {},
        'markdown.extensions.meta': {},
        'markdown.extensions.tables': {},
    },
    'output_format': 'html5',
}

THEME = '../pelican-html5up-striped'
DIRECT_TEMPLATES = ('index',)
SUMMARY_MAX_LENGTH = None

DEFAULT_PAGINATION = 6
PAGINATION_PATTERNS = (
    (1, '{base_name}/', '{base_name}/index.html'),
    (2, '{base_name}/page/{number}/', '{base_name}/page/{number}/index.html'),
)

PROFILE_IMG_URL = '/images/avatar.jpg'
CSS_DARK_BACKGROUND = '#e8e8e8'
CSS_LIGHT_BACKGROUND = '#fff'

ARCHIVE_SAVE_AS = ''
AUTHOR_SAVE_AS = ''

TAG_URL = 'tag/{slug}/'
TAGS_URL = 'tags/'
TAGS_SAVE_AS = 'tags/index.html'

CATEGORY_URL = 'category/{slug}/'
CATEGORYS_URL = 'categories/'
CATEGORYS_SAVE_AS = 'categories/index.html'

CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None


#######################################
# Config options specific to dev-mode:
#######################################

SITEURL = ''
RELATIVE_URLS = True

# Making output generation faster:
TAG_SAVE_AS = ''
CATEGORY_SAVE_AS = ''
FEED_ALL_ATOM = None
STATIC_CHECK_IF_MODIFIED = True # pending pelican 3.8.0 release
STATIC_CREATE_LINKS = True # pending pelican 3.8.0 release
WRITE_SELECTED = []
