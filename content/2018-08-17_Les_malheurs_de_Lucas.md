Title: Les malheurs de Lucas
Slug: Les-malheurs-de-Lucas
Date: 2018-08-17 12:53
Category: Allemagne
---

Coup de froid ? Coup de chaud ? Ingredient mal digéré ?
Notre Herr Lucas s'est senti bien mal hier. Heureusement la journée de vélo prévue était très courte pour arriver à Ülm où nous avons dormi dans une très agréable auberge de jeunesse. Après une nuit de fièvre, Herr Cimon (ou Herr Sigmound) comme Anne-Laure aime à l'appeler, va mieux. 
Les plans aujourd'hui : Prendre le train jusqu'à Regensburg (200km) pour se reposer encore. LaEtitia quant à elle, pète la forme après deux supers nuits !

Et mauvaise nouvelle, Anne-Laure nous a quitté et est actuellement dans un train pour Bale, puis Mulhouse, puis Belfort, puis Besancon !! Un marathon du train ! C'est donc à deux que nous continuons notre périple. 

Ülm était une ville très mignonne avec une gigantesque cathédrale, plein de sculptures d'oiseaux décorées et un quartier de pecheurs très mignon de nuit. Malheureusement, Lucas n'a pas pu en profiter depuis son lit !!!