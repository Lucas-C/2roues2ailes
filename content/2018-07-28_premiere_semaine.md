Title: Première semaine
Slug: premiere-semaine
Date: 2018-07-28 16:30
Category: France
---

7ème jour d'écumage des routes ligériennes, et nous sommes toujours vaillants !

Après avoir passé la journée d'hier à Nevers, nous sommes arrivés cette après-midi à Decize,
d'où nous vous écrivons ce petit billet depuis la bibiliothèque.

Comme vous l'aurez constaté, notre accès internet est limité donc il va faloir vous faire un récapitulatif de cette semaine. 

Aimant la difficulté, nous sommes partis très fatigués le lendemain d'un mariage, dimanche dernier à 16h,
sans jamais avoir testé notre matériel de transport... Evidemment, le pire était à prévoir ! 

20km plus loin, nous voilà obligés de faire un détour à l'extérieur de Blois acheter de nouvelles sacoches, le caddie ne faisant pas du tout notre affaire
(trop instable, et nous ralentissant).
Première petite galère, qui nous a empeché de faire nos 50km de la journée.

Le retard énorme déjà pris, il a fallu pédaler d'autant plus vite pour rattraper tout ça...
Mais la fatigue, l'écrasante chaleur (45°C au soleil pendant 3 jours!!!) et le détour matériel nous empêcherons de rattraper
les 30km manquants pour arriver à notre chambre d'hôtes du 4ème jour.
Nous avons donc, à regret, pris le TER entre Briare et Cosne-sur-Loire (mais chuuuut c'est un secret).

Et le caddie traitre me direz-vous ?? Et bien après 3 jours de mésentente quant à son devenir, nous l'avons déposé en pension chez un aimable orléanais,
que nous ne connaissons de nul part, mais qui nous donnera l'occasion de revenir visiter Orléans à notre retour !! ^^

![Devant la cathédrale à Orléans](/images/P1070106.JPG)

Niveau matériel, tout est au top ! Nous n'avons pas pris trop avec nous et ne manquons de rien pour l'instant.
La liberté du bivouac, nous a permis de pédaler parfois jusqu'à 21h30 à la fraîche, de repartir quand on veut
et c'est un plaisir de pouvoir faire des mini-feux dans notre réchaud dans les champs ou sur les bords de Loire !!
D'ailleurs, l'effort creuse et nous mangeons plutôt bien ;D

![](/images/P1070108.JPG)

![](/images/P1070107.JPG)

Niveau paysage, c'est très sympa. Beaucoup de levées, de canaux, de bords de Loire et de verdure, des villes en bord de Loire :
Blois, Orléans, Chateauneuf-sur-Loire, Gien, Nevers...

Et d'ailleurs quelle ne fut pas notre surprise de voir au détour d'un chemin à Chateauneuf/Loire, Antoine et Béatrice,
l'oncle et la tante niçois(e) de Lucas !!! 

Enfin, hier à Nevers, nous avons pu étrainer notre hamac l'après-midi pour faire la sieste :

![](/images/P1070113.JPG)

Nous avons également en matinée découvert la réponse à une question qui nous taraudait depuis plusieurs jours :
à quoi correspondait donc le "kilomètre zéro" des balises métaliques décroissantes que nous suivions le long de la Loire à Vélo ?
La voici :

![](/images/P1070111.JPG)

Et la réponse : il s'agit du "Bec de l'Allier", l'endroit où cet affluent de la Loire s'y jette.

Nous avons donc officiellement terminé la Loire à vélo, et entamons désormais son prolongement sur l'Eurovélo 6 !

![](/images/P1070112.JPG)

![](/images/P1070110.JPG)

Des bisous et à bientôt pour d'autres nouvelles de notre aventure !

![](/images/P1070109.JPG)
