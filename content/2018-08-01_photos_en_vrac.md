Title: Photos en vrac
Slug: photos_en_vrac
Date: 2018-08-01 16:30
Category: France
---

![Baignade fraîche et bien méritée dans la Loire, à Nevers Plage après plusieurs journées de chaleur](/images/P1070179.JPG)

![Les voies vertes ombragées sont très appréciables](/images/P1070180.JPG)

![Il fait tellement chaud, que les arbres perdent leurs feuilles comme en automne](/images/P1070181.JPG)

![A la découverte des ponts-canaux, ici à Digoin où on a dit au revoir à la Loire](/images/P1070182.JPG)

![Petite sieste dans le hamac](/images/P1070184.JPG)

![Smoothie et milkshake pour commencer une belle journée d'anniversaire](/images/P1070185.JPG)

![A la poursuite des écluses des canaux](/images/P1070186.JPG)

![Des écluses très fleuries sur le chemin](/images/P1070187.JPG)

![Maison à colombages à Châlon-sur-Saone](/images/P1070192.JPG)
