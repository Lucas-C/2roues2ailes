Title: Black Friday
Slug: black-friday
Date: 2018-08-31 11:00
Category: Slovaquie
---

Cet article ne sera pas des plus réjouissant... Apres la grosse tuile des lunettes cassées, nous voici à assumer UNE DES PIRES situations qu'on pouvait imaginer.

A la fin du mois, nous avons passés 24h a Bratislava, en Slovaquie (les seuls 15 Km à effectuer en Slovaquie.
Tout s'était plutot bien passé : chouette visite guidee, super hostel,
visite autour du chateau et repas slovaque a s'en faire péter le bide.

![Une soupe à l'ail dans une enorme boule de pain, un plat à partager avec des spatzles, du choux, du fromage, un plat de chou fleur panés frits et des frites - vous connaissez l'histoire de la grenouille qui veut se faire plus grosse que le boeuf ??](/images/P1070944.JPG)


Bref, apres 24h bien remplies, nous voici repartis vers nos vélos garés devant l'office du tourisme...
quand TOUT A COUP on se rend compte qu'il n'y a plus 2 vélos mais que celui de Laetitia brille par son absence.

Gros coup de stress et de désespoir, et la réalisation de tout ce qu'on vient de perdre... l'équivalent de notre maison depuis plus d'un mois.

Sans vous faire un descriptif complet, en plus du vélo tant chéri de Laetitia, on venait de perdre les sacoches tip-tops qui contenaient TOUS nos vetements,
notre nourriture, notre réchaud, nos gamelles et encore bien plus triste notre carnet de voyage scrupuleusement rempli depuis le début du séjour... et j'en passe...

Un descriptif de cette apres midi tres pourrie :

- apres avoir été redirigés vers un agent, deux numéros de téléphone, et avoir attendu sur place 1h les officiers de police sensés se déplacer...
- nous sommes allés au poste de police pour porter plainte. Personne n'y parlait anglais, ou faisait meme trop l'effort de nous comprendre.
- 1h30 plus tard, nous demandons a un monsieur venu au poste de leur demander ce que je suis sensée atendre et encore combien de temps.
Nous apprenons alors qu'on attend une interprete anglaise, mais qu'elle ne sera là que dans 1h.
Grosse blague, ils auraient pu chercher a se faire comprendre pour ne pas poireauter plus de 2h en tout...
- une fois, l'interprete presente, Laetitia doit déposer une plainte qui durera plus de 1h, elle sort du poste a 19h30, sans etre plus avancée

Heureusement Lucas avait pendant ce temps repris une nuit a l'hostel, fait des recherches pour nos possibilités pour retrouver Nicolas et Kevin le lendemain a Gyor,
en Hongrie (oui ça tombait SUPER mal), racheter des vetements (nous étions en sandales, short, t-shirt) et découvrir que le lendemain était jour férié en Slovaquie
et donc qu'il fallait quitter le pays pour faire le moindre achat car tout serait fermé...

Nous avons donc réservé un bus de Bratislava à Gyor pour le lendemain à l'aube et nous sommes couchés un tantinet déséspérés. 

Le lendemain :

- Arrivée sous un orage et un déluge de fou (toujours en short et sandales)
- Achat d'un nouveau vélo et sacoches pour ne pas abandonner le voyage
- Achat de nouveaux vetements (sous-vetements, pantalons, pulls...)
- Traversée de la ville jusqu'a Decathlon (eh oui, ce magasin est présent dans tous les pays qu'on a traversé et tres fréquenté) pour racheter le minimum de materiel nécéssaire (on laisse tomber le réchaud vu la saison)
- Retour au centre-ville vers 19h30 où Nicolas et Kevin nous attendent déja sous des trombes d'eau
- Repas au restaurant pour se remettre de ces émotions et célébrer la venue des garçons
- Départ pour le camping qui se trouve etre bieeeen plus loin que prévu et ou on arrive trempés, de nuit. Heureusement la dame nous propose un bungalow qu'on accepte avec plaisir !!

Une journée particulierement longue qui faisait un peu beaucoup apres la journée de la veille....  

Le dimanche était un jour nouveau plein d'espoir mais qui commenca avec une crevaison au réveil sur le nouveau pneu du nouveau vélo !! Oui oui oui tout ceci n'est pas une blague, quelqu'un nous a jeté un sort !!!

<style>
#content > div > article > p > img {
    margin: 5rem 0;
}
</style>

