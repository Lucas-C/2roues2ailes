Title: C'est parti !
Slug: cest-parti
Date: 2018-02-07 23:15
Category: preparatifs
---

Bonjour et bienvenue !

Dans ce blog, nous souhaitons partager avec vous cette aventure folle dans laquelle nous allons nous lancer cet été 2018 :
traverser l'Europe à vélo jusqu'à la Mer Noire ! 
Ce projet nous trotte dans la tête depuis plusieurs années et le 22 juillet, nous allons enfin donner le premier coup de pédale d'un voyage de plusieurs mois ! (Lucas a enfin cédé !)

![GIF d'un cycliste joyeux](/images/happy_bike.gif)

Nous espérons pouvoir réaliser certaines étapes avec vous ;) Ce site est là pour nous servir de carnet de voyage, et nous permettre de partager notre organisation et nos aventures. 😊 

Partir trois mois ? Un peu ambitieux pour des athlètes comme nous... 
Mais à raison d'une moyenne de 50km/jour, on y croit. Et puis depuis le Ring of Kerry, on en connait un rayon !! 🚲

Voici donc une première estimation de nos dates de passage tout le long de la piste EuroVelo 6 entre mi-juillet et mi-octobre, en prenant en compte fatigue et visites. 

[![Parcours & dates prévisionnels](/images/ParcoursEurovelo6.png)](images/ParcoursEurovelo6.png)

Bien sûr, des imprévus sont à prévoir. Pas de pression, l'important c'est le voyage, pas la destination. Et puis, ce ne serait pas drôle si on suivait le planning à la lettre 😎

On compte sur vous, à bientôt sur la route.

PS: pour accéder aux commentaires, cliquez sur le le titre de l'article.

PPS: il y avait un petit problème empêchant de laisser un commentaire, ça devrait être corrigé maintenant