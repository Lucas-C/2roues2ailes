Title: Fin prêts, ou presque !
Slug: fin_prêts_ou_presque
Date: 2018-06-19 19:30
Category: preparatifs
---

Pour vous donner un ordre d'idée, voici notre équipement prévisionnel il y a un mois :

 <img src="/images/vetements.jpg">

Après un essai "quantité de matériel vs volume d'une sacoche" lamentablement échoué alors que tout le matériel n'était pas arrivé, 
il a fallu se rendre à l'évidence : il nous faudrait bien plus de volume de sacoche que 40 litres...

Depuis, quelques trucs ont été rajoutés : matelas, tente, sacs de couchage, réchaud à bois...
Et nous avons reçu caddie et sacoches pour un volume total de 110 LITRES !!!! Autant dire que si le but est de tout remplir, on ne va pas rouler vite ;P

Nous avons déjà testé la tente à 2 reprises, et bien qu'elle soit un peu lourde,
on en est très satisfaits !

---

 <img src="/images/trio.jpg">

Le week-end dernier, Anne-Laure, une de nos futurs compagnons de voyage, est venue nous rendre visite à Nantes.

Ça a été l'occasion de se faire une "petite" balade à vélo de chez nous à la Roche Ballue,
qui s'est terminée en un parcours de 40km :

 <img src="/images/parcours_velo.png">

Avez-vous remarqué, à gauche du circuit ?
...
Il n'y a pas de pont, et nous avons traversé la Loire !!

...C'est parce que nous avons pris un **bac**, au niveau d'Indre ;)

---

Nous avons opté pour un réchaud à bois très compact pour ne pas nous surcharger de bouteilles de gaz.
Les essais avec notre scout expérimentée nous ont permis :

- A. D'abandonner l'idée d'allumer un feu avec la pierre à feu (le briquet c'est bien !)
- B. De faire bouillir de l'eau au bout de 5 minutes
- C. D'enfumer tous les voisins un dimanche après-midi ensoleillé ;P
