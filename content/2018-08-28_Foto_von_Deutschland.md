Title: Foto von Deutschland
Slug: Foto-von-Deutschland
Date: 2018-08-28 11:00
Category: Allemagne
---

Nous vous écrivons cette fois de la bibliotheque de Vienne, en Autriche ! Les articles sont très espacés puisque l'accès internet est très
restreint (surtout pour télécharger les photos) et qu'on veut se laisser le peu de temps qu'on a dans les grandes villes pour visiter un peu:

Dans cet article, nous mettons en vrac les photos d'Allemagne que nous avons quitté lundi soir.

(cliquez sur le titre de l'article pour accéder aux légendes des photos et aux commentaires)
 

![Ülm, la ville d'Einstein avec cette fontaine où la tete d'Einstein est collée à un bigorneau, et arrimés à une fusée...](/images/P1070713.JPG)

![En Allemagne, beaucoup de peintures ou fresques sont présentes sur les batiments. Ici, un David contre Goliath à Regensburg](/images/P1070714.JPG)

![Autre graff, découvert pendant la visite guidée de la ville de Regensburg, ville médiévale inscrite au patrimoine mondiale de l'UNESCO](/images/P1070715.JPG)

![A 20 Km de Regensburg, Walhalla ! Un temple érigé par Louis de Bavière sur une colline, imposant sur un thème très athénien](/images/P1070716.JPG)

![Une halte avant bivouac sur une zone humide le lond du Danube où les couleurs de la fin de journée sont autant appréciables que les températures moins caniculaires](/images/P1070717.JPG)

![La Bavière !!! A Straubing, une foire exposition gratuite où nous avons pu apprécier les danses et la musique bavaroises. A 10h, où il faisait déjà chaud, on plaint ces danseurs en costumes traditionnels ](/images/P1070718.JPG)

![Une halte pique-nique chaud au bord d'un étang le soir avant de s'enfoncer derrière les champs de mais !](/images/P1070719.JPG)

![Sur le chemin, quelques clins d'oeil au passage des nombreux vélos. Ici à 25Km de Passau](/images/P1070720.JPG)

![Passau, dernière ville d'Allemagne où se rejoignent trois rivières : le Danube bleu, l'Inn verte et la petite Ilz noire. Ici le point de confluence où le Danube prendra cette couleur verte à partir de Passau !](/images/P1070721.JPG)

![Passau et son festival de jazz gratuit !! Et comme par hasard, un groupe francais "les lapins superstars" qui ont fait danser le public sur une énergie impressionnante](/images/P1070722.JPG)

Un morceau pour vous faire découvrir leur musique : [cliquez ici](https://www.youtube.com/watch?v=oCST9OoCuSs)

![Au camping de Passau : La lessive est toujours un moment délicat où il faut bien faire sécher au plus vite nos affaires encore humides ! ](/images/P1070723.JPG)

![Dernière photo de Passau où nous avons été surpris et époustoufflés par la cathédrale décorée par un artiste italien. Les peintures vives, les sculptures et ornements font de la cathédrale une vraie oeuvre d'art, malgré que de l'extérieur elle soit très moche ! Elle possède d'ailleurs le plus grand orgue situé dans une cathédrale au monde ](/images/P1070724.JPG)

![Le passage en Autriche qui s'est fait sur un énorme barrage où le frontière se trouve au milieu du Danube ](/images/P1070725.JPG)


<style>
article .img-caption {
    position: static !important;
}
#content > div > article > p:nth-child(6) > img {
    transform: rotate(90deg);
    margin: 5rem 0;
}
#content > div > article > p:nth-child(13) > img {
    transform: rotate(-90deg);
    margin: 5rem 0;
}
</style>
