Title: 101 premiers kilomètres sur l'EuroVelo 6!
Slug: 101-premiers-kilometres-sur-l-eurovelo-6
Date: 2018-03-02 12:00
Category: preparatifs
---

Voici nos nouveaux destriers !

<div class="side-by-side-wrapper">
  <figure>
    <img src="/images/P1060737.JPG">
    <figcaption>Un solide VTC autrichien pour Lucas</figcaption>
  </figure>
  <figure>
    <img src="/images/P1060738.JPG">
    <figcaption>Un beau vélo d'origine française pour Laëtitia</figcaption>
  </figure>
</div>

Nous voici partis samedi dernier pour notre premier week-end d'essai !
Deux jours pour relier Angers à Nantes,
ce qui correspond à notre estimation quotidienne moyenne pour cet été.

Résultat : 101km en 7h30 de vélo !

- Angers - Saint Laurent du Mottay le samedi (44km)
- Saint Laurent du Mottay - Nantes le dimanche (57km)

<figure>
  <img src="/images/P1060677.JPG">
  <figcaption>Une vue de la Loire au niveau de la levée à Saint-Florent le Vieil, où la route est un peu dangereuse pour les vélos. Un peu de pluie en perspective !</figcaption>
</figure>

<figure>
  <img src="/images/P1060691.JPG">
  <figcaption>De nombreux chemins inondés nous ont barré la route le dimanche matin. Nous nous sommes un peu obstinés, jusqu'à avoir de l'eau jusqu'aux pieds !</figcaption>
</figure>

<figure>
  <img src="/images/P1060695.JPG">
  <figcaption>Nous avons pédalé un moment aux milieux des prairies inondées, entourés de magnifiques champs de goganes et d'échassiers de passage.</figcaption>
</figure>

Nous avons profité d'une étape à Oudon pour en visiter le château médiéval,
mais le temps n'était pas au rendez-vous pour admirer la vue depuis sa haute tour.

Bilan :

- satisfaits de notre rythme lors de cette première étape.
Plutôt encourageant pour notre planning de cet été !
- beaucoup d'averses, heureusement que nous étions bien équipés.
- des paysages familiers mais vraiment magnifiques.
- une selle douloureuse au bout de 10km.
Sans hésitation notre prochain achat !
- 2 vélos un peu lourd, mais confortables et endurants.
Niveau taille par contre, nous avons décidé de nous les échanger car ça convient mieux.

Et merci beaucoup à Anne & Didier pour leur accueil le samedi soir, et la chasse aux oeufs de Pâques dans le jardin !

<figure>
  <img src="/images/P1060721.JPG">
  <figcaption></figcaption>
</figure>

Nous avons déjà une amie qui se joindra à nous pour deux semaines cet été, du 5 au 19 août à partir de Besançon.
D'autres personnes nous ont dit être motivés pour se joindre à l'aventure :
contactez-nous pour s'organiser en fonction de vos disponibilités,
nous n'attendons que ça ;)

PS: Pour accéder aux commentaires, cliquez sur le titre de l'article.
