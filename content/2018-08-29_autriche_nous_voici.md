Title: Autriche nous voici !
Slug: autriche-nous-voici
Date: 2018-08-29 11:00
Category: Autriche
---
Alors que nous nous apprêtons aujourd'hui à traverser notre 5e pays du périple, la Slovaquie,
voici quelques photos de l'Autriche que nous quittons.

(cliquez sur le titre de l'article pour accéder aux légendes des photos et aux commentaires)
 
Petite note au passage : nous avons testé à nouveau et les commentaires fonctionnent sans problème.
Vous devriez en voir un au pied de notre article précédent.

Lors de notre premier nuit en Autriche nous avons été invité très chaleureusement à prendre l'apéro par Olivia & Rafael,
un couple originaire de Roumanie qui nous ont permis de planter la tente devant chez eux !

Et le lendemain Rafael nous a meme emmené en balade en foret jusqu'à ce perchoir :

![Très belle vue sur le "Schlögener Schlinge", la boucle du Danube, entre Passau et Linz](/images/P1070726.JPG)

![Passionante visite guidée par Marlene du camp de concentration de Mauthausen](/images/P1070727.JPG)

![L'abbaye de Melk](/images/P1070728.JPG)

![Petit déjeuner sur une plage de galets du Danube peu après Melk](/images/P1070730.JPG)

Nous avons eu droit dimanche matin à une visite guidée gratuite du musée d'histoire locale de Zwentendorf : <http://www.museum-zwentendorf.at>

![Une magnifique fontaine à Tulln représentant la demande en mariage d'Attila à Kriemhild, selon la Chanson des Nibelungen](/images/P1070731.JPG)

![Une oeuvre interactive de la collection Green Art à Tulln, où les visiteurs sont invités à construire des colonnes de pierres](/images/P1070732.JPG)

Plus d'infos sur cette oeuvre, en allemand : [NIMM DIR ZEIT](https://www.tulln.at/green-art/green-art-erleben/gartenkunstwerke/gartenkunstwerke-details/?tx_greenart_artworks%5Bartwork%5D=11&tx_greenart_artworks%5Baction%5D=show)

![Le paysage automnal qui commence à s'offrir à nous](/images/P1070733.JPG)

Nous eu un apercu très complet de Vienne via une visite guidée de la ville sur donations organisée par [Good Vienna Tours](https://www.goodviennatours.eu) :

![Le toit de la cathédrale Saint Stephen à Vienne portant le blason de l'Autriche, un aigle bicéphale correspondant aux deux empires sur lesquels régnèrent la lignée des Habsbourg : l'Empire Autrichien et le Saint Empire Romain](/images/P1070734.JPG)

<style>
article .img-caption {
    position: static !important;
}
</style>