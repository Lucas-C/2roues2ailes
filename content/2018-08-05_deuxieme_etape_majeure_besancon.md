Title: Deuxième étape majeure : Besançon
Slug: deuxieme-etape-majeure-besancon
Date: 2018-08-05 13:00
Category: France
---

Après déjà deux semaines passées sur la route, voici quelques nouvelles depuis notre dernier billet.
Nous sommes arrivés hier matin à Besançon, très chaleureusement hébergés par la maman de notre amie Anne-Laure, Evelyne.
C'est l'occasion de nous reposer un peu, de profiter de la fraicheur d'une maison et de vrais lits !

Cette semaine, nous étions donc au centre et à l'est de la France.
Nous avons traversé la très jolie ville de Paray-le-Monial,
passé un petit col entre Palinges et Saint-Vallier,
dégusté un délicieux repas pour l'anniversaire de Lucas à Chagny, "temple de la gastronomie",
visité Chalon sur Saone et son musée de la photographie,
puis la belle ville médiévale de Dole où nous sommes arrivés le soir des "Jeudi en musique",
où nous avons assisté à un petit concert et une belle soirée festive !

Nous sommes très satisfaits de notre rythme jusqu'à maintenant malgré la chaleur :
nous avons fait plus de 50km par jour en moyenne pour le moment,
ce qui nous a permis d'arriver à Besançon avec une journée d'avance !

(cliquez sur le titre de l'article pour accéder aux légendes des photos et aux commentaires)

![Coucher de soleil à Verdun-sur-le-Doubs](/images/P1070248.JPG)

![L'église de Charnay-lès-Chalon](/images/P1070249.JPG)

![Son clocher](/images/P1070250.JPG)

![Une sculpture observée le long de la route près de Pagny-le-Château](/images/P1070251.JPG)

![L'église de Saint-Jean-de-Losne](/images/P1070252.JPG)

![L'écluse débutant le canal du Rhône au Rhin](/images/P1070254.JPG)

![La fontaine souterraine de Dole](/images/P1070255.JPG)

![Le très grand clocher de Dole](/images/P1070257.JPG)

![](/images/P1070262.JPG)

![Nos hôtes à Besançon](/images/P1070267.JPG)
