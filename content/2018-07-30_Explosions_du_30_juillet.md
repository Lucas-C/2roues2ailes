Title: Explosions du 30 juillet
Slug: Explosions_du_30_juillet
Date: 2018-07-30 16:30
Category: France
---

Il y a des jours où il vaut mieux rester couché !! C'est bien ce que Laëtitia a pu se dire pour ce début de semaine...

Après 7 jours de périple, la fatigue s'est faite sentir lundi. C'est ainsi qu'on a tranquillement sorti le hamac pour faire une sieste après un bon repas. 
...QUAND TOUT A COUP...

Lucas fait "oh mince, mince, non, non" et Laëtitia découvre ses jolies lunettes en bois (quasi neuves) cassées en deux.
Elles avaient dû glisser pendant la sieste et les gros pieds maladroits de Lucas ne leur ont donné aucune chance, aucune sommation CRAC : 1ere explosion... 

Nous voilà repartis à pédaler et c'est plus tard, dans la soirée où la fatigue se faisant ressentir d'autant plus (la sieste n'ayant pas été reposante)
que le pire était à venir : depuis des heures au milieu de rien, un lundi soir, nous n'avons pas été capable de trouver un seul lieu pour manger,
même pas une boulangerie... BAM. S'étant résolus à trouver un endroit pour camper et manger tristement notre ration de survie pour les au-cas où d'urgence,
voilà Laëtitia qui n'ayant plus ses yeux n'a pas vu les dangereux gravillons qui la menaçaient...

CABOUM, voici le vélo parti en dérapage, et la madame par terre avec un genou tout cassé...
Il était grand temps de trouver un endroit et se coucher, la semaine commençait bien...

![](/images/P1070177.JPG)


![](/images/P1070178.jpg)

