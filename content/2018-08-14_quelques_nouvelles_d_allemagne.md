Title: Quelques nouvelles d'Allemagne
Slug: quelques-nouvelles-d-allemagne
Date: 2018-08-14 16:00
Category: Allemagne
---

Nous vous ecrivons cette fois de la bibliotheque de Sigmaringen, en Allemagne,
ou les claviers d'ordinateur ont une disposition quelque peu exotique !
Pas simple donc de s'y retrouver pour ecrire, encore moins pour inserer des caracteres accentues, donc tant pis !

(cliquez sur le titre de l'article pour accéder aux légendes des photos et aux commentaires)

Depuis notre dernier petit article, nous avons assiste a la fete pour la paix a Besancon,
notre amie Anne-Laure nous a fait visiter la ville le dimanche,
puis nous sommes repartis avec elle sur la route le lundi matin.

![Le depart de Besancon](/images/P1070451.JPG)

Nous avons a nouveau suivi un nouveau decompte mysterieux peint au sol...
Jusqu'a croiser un fameux cours d'eau au kilometre zero.

![Le kilometre zero](/images/P1070452.JPG)

![Une cigogne alsacienne](/images/P1070453.JPG)

C'etait en fait l'indication que nous quittions le Doubs pour entrer dans le territoire de Belfort, puis en Alsace,
ou nous avons croise plein de cigognes !

![Entre Nantes et Budapest](/images/P1070454.JPG)

Destination ensuite : Mulhouse, ou ils mettent des pulls aux arbres et ou nous avons fait le tour des murs peints de la ville.

![](/images/P1070455.JPG)

![](/images/P1070456.JPG)

![](/images/P1070457.JPG)

![](/images/P1070458.JPG)

![](/images/P1070459.JPG)

![](/images/P1070460.JPG)

Derniere etape avant de sortir de France: Saint-Louis,
ou nous avons recupere les lunettes de Laetitia reparees,
renvoyees par la Poste !

![Le colis salvateur](/images/P1070461.JPG)

Nous sommes ensuite entres en Suisse par Bale,
ou la route de l'Eurovelo etait tres mal indiquee,
et la nourriture hors de prix !

![Lumiere de crepuscule a Bale](/images/P1070462.JPG)

Autant le dire tout de suite : nous n'avons pas particulierement apprecie la Suisse,
son paysage tres dense et industrialise, ses voies pour cyclistes en bordure de nationales, ses chemins caillouteux, son relief, ses ovnis...

![OVNI dans un champ en Suisse](/images/P1070463.JPG)

![Petites cabanes en Suisse](/images/P1070464.JPG)

![A velo en Suisse](/images/P1070465.JPG)

![En Suisse, interdit de porter son casque de velo dans les jardins publics](/images/P1070466.JPG)

![Le Rhin](/images/P1070467.JPG)

![](/images/P1070468.JPG)

Une etape magnifique tout de meme : les "Reinfall", les grandes chutes d'eau sur le Rhin :

![Reinfall](/images/P1070469.JPG)

![Reinfall](/images/P1070470.JPG)

![Reinfall](/images/P1070471.JPG)

Et enfin, passage en Allemagne !

![Reinfall](/images/P1070472.JPG)

Nous longeons alors le lac de Constance, appele la bas "Bodensee",
pour ensuite attaquer la partie la plus pentue de notre voyage :
les montagneuses forets avant d'atteindre Tuttlingen, ou nous rejoignons le Danube !

![Victoire !](/images/P1070473.JPG)

A partir de la, la route a ete tres agreable jusqu'a aujourd'hui.

![Une etrange sculpture dans le Danube](/images/P1070474.JPG)

![Attaque de loches pendant la nuit](/images/P1070475.JPG)

![Le Danube magnifique](/images/P1070476.JPG)

<style>
#content > div > article > p:nth-child(13) > img,
#content > div > article > p:nth-child(15) > img,
#content > div > article > p:nth-child(16) > img,
#content > div > article > p:nth-child(17) > img {
    transform: rotate(90deg);
    margin: 5rem 0;
}
</style>
