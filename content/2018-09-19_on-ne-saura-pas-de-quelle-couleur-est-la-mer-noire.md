Title: On ne saura pas de quelle couleur est la mer noire 
Slug: on-ne-saura-pas-de-quelle-couleur-est-la-mer-noire
Date: 2018-09-19 12:00
Category: Croatie
---

(cliquez sur le titre de l'article pour accéder aux légendes des photos et aux commentaires)

Après une longue absence, voici probablement les derniers articles du blog pendant le trajet  :) 
Depuis Budapest, nous avons repris la route, toujours sur l'eurovélo 6 mais avec bien des difficultés quant aux panneaux !!
On savait que l'aménagement était surtout fait jusqu'à Budapest mais la suite a été compliqué ! Nous avons passé 3 jours à nous perdre à chaque fois pendant 1h30 et revenir au point initial, 
donc on était loin des 50km de moyenne !!


Voici quelques photos prises entre Budapest (Hongrie) et Osijek (Croatie) :



![en Hongrie, sur une levée proche du Danube, Laetitia sur son nouveau vélo (qui est beaucoup moins bien que l'ancien : freins, selle, fourche nécéssitent une révision)](/images/P1080251.JPG)

![Une deuxième crevaison pour Laetitia (à chaque fois sur le nouveau vélo) et 2 crevaisons aussi au compteur de Lucas ! Autant dire qu'on est devenus rodés quant aux réparations et aux rustines](/images/P1080252.JPG)

![Une des nombreuses fausses pistes où le manque de signalisation nous a fait nous perdre. Ici, à Baja en Hongrie où nous avons persévéré 1h30 jusqu'à avoir des orties jusqu'aux genoux !](/images/P1080253.JPG)

![Mohacs, Hongrie : le musée du Buso (se prononce Bochou) qui est un personnage symbolisant la sortie de l'hiver lors du festival annuel en février, qui dure 6 jours et où les participants font la fête anonymement sous ce costume. A la fin des 6 jours, ils brûlent un cercueil symbolisant l'hiver.](/images/P1080254.JPG)

![Après le passage de la frontière, avec contrôle des papiers, nous sommes accueillis en Croatie par plusieurs panneaux de bienvenue, mais aussi la piste eurovélo que nous allons suivre avec les distances. Les nouveaux panneaux seront bleus !! Nous arrivons par le nord est du pays, dans la région de la Slavonie](/images/P1080255.JPG)

![A l'éco-centre de Zlatna Greta, petite virée en canoé sur un canal, en fin de journée. Parce que le vélo c'est pas assez !](/images/P1080256.JPG)

![Ruée de moutons autogérés sur la levée. On ne doit notre survie qu'à notre courage et à notre capacité à nous camoufler en broutant de l'herbe](/images/P1080257.JPG)

![Passerelle aménagée sur plusieurs kilomètres dans le parc national de Kopacki Rit, où nous avons passé une nuit sous tente entourés de cerfs qui bramaient toute la nuit, mais chuuuut ](/images/P1080258.JPG)


Au passage, voici une très belle fresque réalisée à Osijek, inspirée par une chanson de Paul Simon et l'histoire de la ville : [Rhythm of the Saints by Lunar](http://www.ekosystem.org/photo/939827)


Finalement, nous décidons de changer nos plans de voyage. La fatigue, le timing et la météo nous motivent à ne pas pousser jusqu'à la Mer Noire. 
Déjà très à l'est, il fait nuit à 18h30 !

Notre nouveau plan :

- Aller à Zagreb en train et visiter 2 jours
- Revenir en train jusqu'à Paris avec des correspondances à : Munich - Zürich - Bâle - Mulhouse
- Tester l'eurovélo 3 (la scandibérique : Scandinavie - Espagne) en cours de finalisation, sur la portion de Paris à Orléans. Et.... la boucle est bouclée, nous repassons récupérer notre caddie !!
- Reprendre l'eurovélo 6 jusqu'à Angers, la seule section qu'on n'a pas faite de la Loire à vélo

![Zagreb : tous les jours à midi, un coup de canon est tiré de cette tour pour indiquer aux églises qu'il faut sonner l'heure. Une tradition qui se perdure depuis un siècle](/images/P1080260.JPG)

![Le retour : 8 minutes de correspondance à Bâle : chaud chaud mais pas impossible](/images/P1080261.JPG)

![Les nouveaux panneaux à suivre à partir de Paris : l'eurovélo 3, qui n'est pas encore très bien indiqué](/images/P1080262.JPG)

![Le retour en France a été TRES difficile, puisque nous passions de 30°C de jour à Zagreb à 4°C de nuit en banlieue parisienne. Ici, le long de la Seine à Ris-Orangis où la nuit s'annonce frigorifique. Fallait bien tester le matériel en conditions extrêmes, nous avons survécu haut la main !!!](/images/P1080263.JPG)

![Une agréable surprise, la Seine nous offre de très beaux paysages marqués par le début de l'automne. ](/images/P1080264.JPG)

![Le long du canal du Loing, à 10km avant Nemours : une maison abandonnée d'écluse s'annonce comme un site de camping providentiel. Ce sera l'occasion d'inventer la mouflette : la chaussette qui sert aussi de moufle.](/images/P1080265.JPG)

![Comme l'indique ce panneau, les betteraves locales sont hargneuses. Nous leur avons échappé en zig-zagant](/images/P1080266.JPG)

![Dans la forêt domaniale d'Orléans où le paysage est magnifique et le soleil très agréable ce jour-là](/images/P1080267.JPG)




<style>
article .img-caption {
    position: static !important;
}
</style>