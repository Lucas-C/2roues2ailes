Title: De Vienne à Budapest
Slug: de-vienne-a-budapest
Date: 2018-09-09 19:30
Category: Hongrie
---

(cliquez sur le titre de l'article pour accéder aux légendes des photos et aux commentaires)

![La façade du musée Hundertwasser à Vienne. Hundertwasser : un homme tres inspirant qui a travaillé avec un architecte pour des constructions farfelues  ou la nature a une grande place, faire des arbres des locataires des maisons, rendre les sols non plats et faire des batiments beaux et colorés ](/images/P1070942.JPG)

![La 5e édition de l'expo "Wire Zoo" au Palace du Primat, a Bratislava. Une exposition entiere montrant des animaux en fils de fer, un travail tres précis et un joli rendu](/images/P1070943.JPG)

![Zizag entre les énormes flaques apres Gyor, Hongrie avec Nicolas et Kevin. Les orages des derniers jours et les routes se déteriorant a vue d'oeil, nous avons été bien ralentis pour cette semaine qui commencait avec les garcons](/images/P1070945.JPG)

![Petite improvisation à la harpe cycliste...](/images/P1070946.JPG)

![..pendant que Kevin pose sa premiere rustine. Premiere reussie !! Un sans faute !](/images/P1070947.JPG)

![Dégustation de langos, une spécialité locale à base de pain frit, avec ce l'ail, de la creme et du fromage, un plat extremement diététique vous l'aurez deviné pour seulement 2euros !!](/images/P1070948.JPG)

![Panorama sur Esztergom depuis la coupole de sa gigantesque basilique ou comme vous pouvez le voir le temps tres nuageux et humide ne nous a pas quitté pendant les premiers jours avec les garcons](/images/P1070949.JPG)

![Traversée obligée en bac sur le Danube pour 10minutes, pour pouvoir rejoindre la piste cyclable qui se poursuit sur l'autre rive](/images/P1070950.JPG)

![Arrivée a Budapest : Les remparts de la forteresse de Buda tres jolis mais aussi tres touristique, la partie ouest de Budapest beaucoup plus riche par rapport a la ville a l'est du Danube : Pest: 2 villes reliés par le pont de chaine !](/images/P1070952.JPG)

![Sculpture médiévale typique, dite "des 3 singes" en representation ephemere par des artistes d'un nouveau genre](/images/P1070953.JPG)

![Vue sur Buda et Pest depuis le mont Gellért, ou l'ancienne citadelle surplombe la ville. On peut voir une capitale extremement étendue mais ou les rues sont aussi tres larges et agréables. Comme dans beaucoup de capitale, la circulation est laborieuse, voire dangereuse !](/images/P1070954.JPG)

![Fresque murale comportant le mot familier hongrois pour dire "vélo", vue lors d'une visite guidée sur le street art dans la partie de Pest](/images/P1070955.JPG)


<style>
article .img-caption {
    position: static !important;
}
#content > div > article > p:nth-child(4) > img,
#content > div > article > p:nth-child(8) > img{
    transform: rotate(-90deg);
    margin: 5rem 0;
}
#content > div > article > p:nth-child(13) > img,
#content > div > article > p:nth-child(15) > img{
    transform: rotate(90deg);
    margin: 5rem 0;
}
</style>
