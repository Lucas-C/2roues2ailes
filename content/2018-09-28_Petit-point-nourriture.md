Title: Petit point nourriture
Slug: Petit-point-nourriture
Date: 2018-09-28 12:00
Category: France
---

(cliquez sur le titre de l'article pour accéder aux légendes des photos et aux commentaires)

Durant ces deux derniers mois et demi, nous estimons avoir plutôt bien mangé. La nourriture étant bien moins chère dans tous les pays traversés par rapport à la France 
(la Croatie étant de façon surprenante plutôt chère). 

Certes, nos parents trouveront que nous avons un peu maigris (et surtout pris du muscle ;p), mais ne vous inquiétez pas nous revenons entiers, en pleine santé, et pas du tout malnutris !
Les deux derniers pays, Hongrie et Croatie, n'ayant pas une gastronomie nous correspondant - très gras, très salé et surtout beaucoup de viande - 
nous étions très contents de rentrer en France et nous sommes rués dans le premier supermarché pour manger du taboulé !! Voici 3 photos témoignant de nos péripéties alimentaires.
 
 
 
![Nicolas et Kevin nous ont laissé leur ration de survie après leur départ. Nous nous sommes empressés de les déguster !!! Aligot et lentilles lyophilisées avec un bol de flageolet en sauce !!! BON APPETIT BIEN SÛR](/images/P1080249.JPG)

![LE PIRE repas du trajet : des boîtes de conserve avec des lentilles et d'autres trucs... Et d'après l'étiquette, de la garniture de pizza ??!! Même notre chat n'en aurait pas voulu !!](/images/P1080250.JPG)

![Un excellent restaurant, Kod Ruže à Osijek, où Lucas a commandé une soupe à la farine : elle était délicieuse et servie dans une miche de pain](/images/P1080259.JPG)






<style>
article .img-caption {
    position: static !important;
}
</style>