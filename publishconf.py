#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# This file is only used if you use `make publish` or
# explicitly specify it as your config file.

import os
import sys
sys.path.append(os.curdir)
from pelicanconf import *

SITEURL = 'https://2roues2ailes.chezsoi.org/'
#SITEURL = 'http://lucas-c.frama.io/2roues2ailes/'
RELATIVE_URLS = False

THEME = 'pelican-html5up-striped' # from git submodule

# Reverting speed optimization settings
TAG_SAVE_AS = 'tag/{slug}/index.html'
CATEGORY_SAVE_AS  = 'category/{slug}/index.html'
FEED_ALL_ATOM = 'feeds/all.atom.xml'
WRITE_SELECTED = []

DELETE_OUTPUT_DIRECTORY = True
